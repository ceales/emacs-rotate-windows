# emacs-rotate-windows

An elisp package to provide window management redolent of a stacking tiling
window manager.

It can configure the on-screen windows as either a left main, or top main, view.
It then adds functions to swap and rotate the buffers between the windows.

Inspired by the eamcs-rotate package by Daichi Hirata -
[https://github.com/daichirata/emacs-rotate](https://github.com/daichirata/emacs-rotate)

## API

+ `vertical-left-layout`: rearrange the windows in the current frame, such
that the left hand-side is a single tall window, and the other windows are
stacked on the right.

+ `vertical-let-layout-n`: prompt for the number of windows and then configure
the windows in the current frame, such that the left hand-side is a single tall
windows, and the other `n-1` windows are stacked on the right.

+ `horizontal-top-layout`: rearrange the windows in the current frame, such that
the top is a single tall window, and the other windows are stacked below.

+ `horizontal-let-layout-n`: prompt for the number of windows and then configure
the windows in the current frame, such that the top is a single tall windows,
and the other `n-1` windows are stacked below.

+ `rotate-buffers-clockwise`: rotate the buffers in the current windows by 1
  place clockwise. The focus stays in the current window. 

+ `rotate-buffers-anticlockwise`: rotate the buffers in the current windows by 1
  place anticlockwise. The focus stays in the current window. 
  
+ `rotate-buffers-ff-clockwise`: rotate the buffers in the current windows by 1
  place clockwise. The focus follows the currently active buffer.

+ `rotate-buffers-ff-anticlockwise`: rotate the buffers in the current windows by 1
  place anticlockwise. The focus follows the currently active buffer.
  
+ `swap-current-buffer-to-front`: switch the buffer in the currently active
  window with the buffer in the 1st window and switch focus to the 1st window.
  
+ `rotate-current-buffer-to-front`: rotate the buffers until the currently
  active buffer is in the 1st window, and switch focus to the 1st window.
  
 ### Copyright
 (c) Craig Eales 2020
