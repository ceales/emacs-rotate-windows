;;; rotate-windows.el --- Rotate the layout of emacs

;; Copyright (C) 2020  craig.eales

;; Author: craig.eales <craig.eales at gmail.com>
;; Version: 0.2.0
;; Keywords: window, layout
;; URL: https://gitlab.com/ceales/emacs-rotate-windows

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Commentary:

;; The code is inspired by emacs-rotate by daichi.hirata
;; URL: https://github.com/daichirata/emacs-rotate

;;; Code:
(require 'dash)
(require 'cl-macs)

(defun rotate:window-list ()
  "Get a list of the currently active windows."
  (window-list nil nil (minibuffer-window)))

(defun rotate:select-first-window ()
  "Select the 1st window in the current frame."
  (select-window (car (rotate:window-list))))

(defun rotate:select-next-window ()
  "Select the next window (clockwise) in the current frame."
  (select-window (next-window)))

(defun rotate:buffer-details-list ()
  "Get a list of the buffers in each window along with the window start position and the point."
  (mapcar (lambda (w) (list (window-buffer w) (window-start w) (window-point w))) (rotate:window-list)))

(defun rotate:set-window-buffer-details (window buffer-details)
  "Set the contents of WINDOW to the buffer/start/position in BUFFER-DETAILS."
  (apply (function set-window-buffer-start-and-point) window buffer-details))

(defun rotate:set-buffers (buffer-detail-list)
  "Set each window to the corresponding buffer/start/point in BUFFER-DETAIL-LIST."
  (cl-loop for w in (rotate:window-list)
           for b in buffer-detail-list
           do (rotate:set-window-buffer-details w b)))

(defun rotate:reorder-buffers (reorder-function)
  "Apply REORDER-FUNCTION to reorder the current buffers in the current frame."
  (rotate:set-buffers (funcall reorder-function (rotate:buffer-details-list))))

(defun rotate:swap-to-front (n l)
  "Given the list L swap element N with the element at the front of the list."
  (if (= n 0)
      l
    (append
     (list (nth n l))
     (-take (- n 1) (cdr l))
     (list (car l))
     (-drop (+ 1 n) l))))

(defun rotate:current-active-window-index ()
  "Return the index of the current active window."
  (-elem-index (selected-window) (rotate:window-list)))

(defun rotate:extend-list (l n elem)
  "Append copies of ELEM to list L until L has length N."
  (let ((ll (length l)))
    (if (< ll n)
        (append l
                (make-list (- n ll) elem))
      l)))

(defun rotate:set-up-vertical-left-layout (num-windows)
  "Replace the current windows with a vertical left layout of NUM-WINDOWS."
  (delete-other-windows)
  (unless (= num-windows 1)
    (split-window-horizontally)
    (rotate:select-next-window)
    (let* ((other-windows (- num-windows 1))
           (height (/ (window-height) other-windows)))
      (dotimes (_ (- other-windows 1))
        (split-window-vertically height)
        (rotate:select-next-window)))
    (rotate:select-first-window)))

(defun rotate:set-up-horizontal-top-layout (num-windows)
  "Replace the current windows with a horizontal top layout of NUM-WINDOWS."
  (delete-other-windows)
  (unless (= num-windows 1)
    (split-window-vertically)
    (rotate:select-next-window)
    (let* ((other-windows (- num-windows 1))
           (width (/ (window-width) other-windows)))
      (dotimes (_ (- other-windows 1))
        (split-window-horizontally width)
        (rotate:select-next-window)))
    (rotate:select-first-window)))

(defun rotate:apply-layout (layout num-windows)
  "Apply the LAYOUT with NUM-WINDOWS in the frame."
  (let ((bdl (rotate:buffer-details-list)))
    (funcall layout num-windows)
    (rotate:set-buffers (rotate:extend-list bdl num-windows (car bdl)))
    ))

;;;###autoload
(defun vertical-left-layout-n (num-windows)
  "Set up the window layout to vertical main - with NUM-WINDOWS in the frame.

If NUM-WINDOWS is greater than the current number of windows then
the new windows are set to the buffer in the 1st window.

If NUM-WINDOWS is less than the current number of windows then
the trailing buffers are dropped."
  (interactive "nNumber of Windows:")
  (rotate:apply-layout (function rotate:set-up-vertical-left-layout) num-windows))

;;;###autoload
(defun vertical-left-layout ()
  "Set up the window layout to vertical main."
  (interactive)
  (vertical-left-layout-n (count-windows)))

;;;###autoload
(defun horizontal-top-layout-n (num-windows)
  "Set up the window layout to horizontal main - with NUM-WINDOWS in the frame.

If NUM-WINDOWS is greater than the current number of windows then
the new windows are set to the buffer in the 1st window.

If NUM-WINDOWS is less than the current number of windows then
the trailing buffers are dropped."
  (interactive "nNumber of Windows:")
  (rotate:apply-layout (function rotate:set-up-horizontal-top-layout) num-windows))

;;;###autoload
(defun horizontal-top-layout ()
  "Set up the window layout to horizontal main."
  (interactive)
  (horizontal-top-layout-n (count-windows)))

;;;###autoload
(defun rotate-buffers-clockwise ()
   "Rotate the buffers (clockwise) within the current windows."
  (interactive)
  (rotate:reorder-buffers (lambda (bl) (-rotate 1 bl))))

;;;###autoload
(defun rotate-buffers-anticlockwise ()
   "Rotate the buffers (anti-clockwise) within the current windows."
  (interactive)
  (rotate:reorder-buffers (lambda (bl) (-rotate -1 bl))))

;;;###autoload
(defun rotate-buffers-ff-clockwise ()
  "Rotate the buffers (clockwise) within the current windows and the focus follows the rotation."
  (interactive)
  (rotate-buffers-clockwise)
  (rotate:select-next-window))

;;;###autoload
(defun rotate-buffers-ff-anticlockwise ()
  "Rotate the buffers (clockwise) within the current windows and the focus follows the rotation."
  (interactive)
  (rotate-buffers-anticlockwise)
  (select-window (previous-window)))

;;;###autoload
(defun swap-current-buffer-to-front ()
  "Swap the buffer of the current window with the buffer in the 1st window, and switch focus to the 1st window."
  (interactive)
  (rotate:reorder-buffers
   (lambda (bl)
       (rotate:swap-to-front (rotate:current-active-window-index) bl)))
  (rotate:select-first-window))

;;;###autoload
(defun rotate-current-buffer-to-front ()
  "Rotate the windows (clockwise) until the buffer in the current window is in the 1st window position."
  (interactive)
  (let ((window-index (rotate:current-active-window-index)))
    (unless (= window-index 0)
      (rotate:reorder-buffers (lambda (bl) (-rotate (- (count-windows) window-index) bl)))
      (rotate:select-first-window))))

(provide 'rotate-windows)
;;; rotate-windows ends here
